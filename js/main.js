$(document).ready(function () {
	var showChart = true;
	if($('#chartdiv').length){
		var chart = AmCharts.makeChart( "chartdiv", {
			"type": "gantt",
			"mouseWheelScrollEnabled": true,
			"mouseWheelZoomEnabled": false,
			"plotAreaFillColors": "#000000",
			"backgroundColor": "#000000",
			"theme": "light",
			"marginRight": 70,
			"labelAnchor": 'start',
			"period": "DD",
			"dataDateFormat": "YYYY-MM-DD",
			"columnWidth": 0.25,
			"valueAxis": {
				"type": "date",
				"autoGridCount": false,
				"gridCount": 0,
			},
			"graph": {
				"balloonText": "<b>[[category]]</b>",
				"fillAlphas": 1,
				"fillToAxis": "#000000",
				"fillToGraph": "#000000",
				"labelColorField": "#000000",
				"lineAlpha": 1,
				"lineColor": "#fff",
				"balloonFunction": function(graphDataItem, graph) {
					if(graphDataItem.index == 1){
						return '<b>'+graphDataItem.category+'</b>'+graph.balloonText+'<ul style=\'text-align: left\'><li>Pricing</li><li>PolicyWording</li><li>Rules and regulations</li><li>Business rules</li><li>MID</li><li>Data Exchange API</li><li>Fulfilment documents</li></ul>';
					}else {
						return '<b>'+graphDataItem.category+'</b>';
					}
				},
			},
			"rotate": true,
			"categoryField": "category",
			"segmentsField": "segments",
			"colorField": "color",
			"startDateField": "start",
			"endDateField": "end",
			"durationField": "2019",
			"dataProvider": [ {
				"category": "Secure Domain name + SSL certificate",
				"segments": [ {
					"start": "2018-01-01",
					"end": "2018-01-10",
					"color": "#ffa503"
				} ]
			}, {
				"category": "Documentation",
				"name": "\n•Pricing\n•PolicyWording\n•Rules and regulations\n•Business rules\n•MID\n•Data Exchange API\n•Fulfilment documents",
				"segments": [{
					"start": "2018-01-05",
					"end": "2018-02-05",
					"color": "#2ed672",
				},],
				"anchor":"start"
			}, {
				"category": "Language Translations",
				"description": "Test",
				"segments": [ {
					"start": "2018-02-05",
					"end": "2018-02-20",
					"color": "#ff6348",
				}]
			}, {
				"category": "Designs of white label + in path booking",
				"segments": [ {
					"start": "2018-01-01",
					"end": "2018-02-20",
					"color": "#1f8fff",
				}]
			}, {
				"category": "Solution Implementation (white label)",
				"segments": [ {
					"start": "2018-02-20",
					"end": "2018-03-10",
					"color": "#ff4657",
				}]
			}, {
				"category": "Solution Implementation (API)",
				"segments": [ {
					"start": "2018-02-20",
					"end": "2018-03-6",
					"color": "#3842fa",
				}]
			}, {
				"category": "Data Exchange/Reporting",
				"segments": [ {
					"start": "2018-03-10",
					"end": "2018-04-5",
					"color": "#f0dda1",
				}]
			} ],
			"valueScrollbar": {
				"autoGridCount": true
			},
			"chartCursor": {
				"cursorColor": "#3032c1",
				"valueBalloonsEnabled": false,
				"cursorAlpha": 0,
				"valueLineAlpha": 0.5,
			},
		} );
	}
	$('#fullpage-wrapper').fullpage({
		afterLoad: function(anchorLink, index){
			var current = index <= 9 ? '0' + index : index ;
			if($('.slider-nav .amount').text() == ''){
				var amount = $('section.section').length <= 9 ? '0' + $('section.section').length : $('section.section').length;
				$('.slider-nav .amount').text(amount);
			}
			if(index == 1){
				$('#moveLeft').addClass('disabled');
			}else{
				$('#moveLeft').removeClass('disabled');
			}
			if($('#chartdiv').length && index == 2){
				$.notify.addStyle('foo123', {
					html:
					"<div class='clearfix'><div class='image' data-notify-hint='hint'><img src='img/hint.gif'></div>" +
					"<div class='title' data-notify-html='title'/>" +
					"</div>" +
					"</div>"
				});
				$.notify({
					title: 'Highlight to zoom',
					hint: 'img/hint.gif'
				}, {
					style: 'foo123'
				});

				$('#chartdiv').on('mouseover',function () {
					$.fn.fullpage.setAllowScrolling(false);
				});
				$('#chartdiv').on('mouseout',function () {
					$.fn.fullpage.setAllowScrolling(true);
				});

			}
			if(index == $('#fullpage-wrapper section').length){
				$('#moveRight').addClass('disabled');
			}else{
				$('#moveRight').removeClass('disabled');
			}
			$('.slider-nav span.current').text(current);
			if($('.fp-viewing-2 #container_chart').length && showChart){
				showChart = false;
				Highcharts.chart('container_chart', {
					chart: {
						panning: true,
						type: 'spline',
						backgroundColor: 'transparent',
					},
					navigation: false,
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						labels: false,
						tickWidth: 0,
					},
					yAxis: {
						reversed: false,
						title: {
							enabled: false,
							text: ''
						},
						labels: {
							format: '{value} M'
						},
						maxPadding: 0.05,
						showLastLabel: true,
					},
					legend: {
						enabled: false
					},
					plotOptions: {
						spline: {
							marker: {
								enable: false
							},
							color: '#3c3ec3',
							enableMouseTracking: false,
						}
					},
					series: [{
						type: 'spline',
						data: [
							{y:4},
							{y:7,color: 'transparent'},
							{y:5.5},
							{y:4.5,color: 'transparent'},
							{y:8}
						],
					}]
				});
			}
			if($('body.fp-viewing-2 #myChart').length){
				var ctx = document.getElementById('myChart').getContext('2d');
				var progress = document.getElementById('animationProgress');
				var myDoughnutChart = new Chart(ctx,
					{
						"type":"doughnut",
						"data":{
							"datasets": [
								{
									"label":"My First Dataset",
									"data":[12,12,4,27,45],
									"backgroundColor":["#5659ca","#7f80d6","#a6a7e0","#ceceeb","#3033c1"],
									"borderColor":["#5659ca","#7f80d6","#a6a7e0","#ceceeb","#3033c1"],
									"hoverBorderWidth": 0,
									"borderWidth": 0,
									"hoverBorderColor": ["#5659ca","#7f80d6","#a6a7e0","#ceceeb","#3033c1"],
									"hoverBackgroundColor": ["#5659ca","#7f80d6","#a6a7e0","#ceceeb","#3033c1"],
								}
							]
						},
						options: {
							tooltips: {enabled:false},
							"cutoutPercentage": 60,
						}
					});
			}
		}
	});
	$(document).on('click', '.regional-slider .regional-slide3 .legend>div:not(.off)', function(){
		$('.dot',this).css('background','white');
		$('.regional-slide3 table tr td div.'+$(this).attr('class')+'').hide();
		$(this).addClass('off');

	}).on('click', '.regional-slider .regional-slide3 .legend>div.off', function(){
		$('.dot',this).attr('style','');
		$(this).removeClass('off');
		$('.regional-slide3 table tr td div.'+$(this).attr('class')+'').show();
	}).on('click', '#moveRight', function(){
		$.fn.fullpage.moveSectionDown();
	}).on('click', '#moveLeft', function(){
		$.fn.fullpage.moveSectionUp();
	}).on('click', 'a[href="#air-asia-block"]', function(){
		$.fn.fullpage.moveTo(7);
		return false;
	}).on('click', '.growth-links-block div.link a', function(){
		$.fn.fullpage.moveTo($(this).data('slide'));
		return false;
	}).on('click', '.back-link a', function(){
		$.fn.fullpage.moveTo(2);
		return false;
	}).on('click', 'a[href="#lufthansa-block"]', function(){
		$.fn.fullpage.moveTo(8);
		return false;
	}).on('click', '.tab-list-block li',function () {
		if($('div.tab-content[data-target="'+$(this).data('target')+'"]').length){
			$('.tab-list-block li').removeClass('active');
			$(this).addClass('active');
			$('div.tab-content').removeClass('active');
			$('div.tab-content[data-target="'+$(this).data('target')+'"]').addClass('active');
		} else {
			alert('Content coming soon');
		}
	}).on('mouseenter', '.asia-map svg path',function () {
		if ($(this).data('country') !== undefined) {
			$('.tooltip span.name').text($(this).data('country'));
			$('.tooltip span.year').text($(this).data('year'));
			$('.tooltip img').attr('src',$(this).data('img'));
			$('.tooltip').fadeIn(500);
		}
	}).on('mouseleave', '.asia-map svg',function () {
			$('.tooltip').fadeOut(1000);
	});
	$(".menu-toggle").on("click",function(){
		$(".sp-nav").addClass("menu-on-default");
		$(this).addClass("menu-on");
		$("main").addClass("menu-on");
		$(".header .logo").addClass("menu-on");
	});
	$(".menu-close").on("click",function(){
		$(".sp-nav").removeClass("menu-on-default");
		$(".menu-toggle").removeClass("menu-on");
		$("main").removeClass("menu-on");
		$(".header .logo").removeClass("menu-on");
	});
	$(".sp-nav .slidedown").on("click",function(){
		$(this).toggleClass("on");
	});
	var slider = $('.owl-carousel');
	slider.owlCarousel({
		items: 4,
		margin: 30,
		smartSpeed:450,
        autoWidth: true,
		dots: false
	});
	var redraw = false;
	$(document).on('click', '.owl-item.active:last', function(){
		var position = $(this).index();
		if(position != $('.owl-item').length - 1){
			slider.trigger('to.owl.carousel', position);
			return false;
		}
	}).on('click', '.before-after .after .image', function(){
		$('.image-popup',$(this).parents('section')).addClass('open');
	}).on('click', '.image-popup', function(){
		$(this).removeClass('open');
	}).on('click', '.regional-slide3 .countries', function(){
		$(this).toggleClass('open');
		$('.regional-slide3').toggleClass('show-table');
		if($(this).hasClass('open')){
			$.fn.fullpage.setAllowScrolling(false);
		}else{
			$.fn.fullpage.setAllowScrolling(false);
		}
	}).on('click', '.table-block .inner', function(){
		$('.table-popup',$(this).parents('section')).addClass('open');
	}).on('click', '.table-popup', function(){
		if(redraw){
			Highcharts.chart('container_chart', {
				chart: {
					panning: true,
					type: 'spline',
					backgroundColor: 'transparent',
				},
				navigation: false,
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					labels: false,
					tickWidth: 0,
				},
				yAxis: {
					reversed: false,
					title: {
						enabled: false,
						text: ''
					},
					labels: {
						format: '{value} M'
					},
					maxPadding: 0.05,
					showLastLabel: true,
				},
				legend: {
					enabled: false
				},
				plotOptions: {
					spline: {
						marker: {
							enable: false
						},
						color: '#3c3ec3',
						enableMouseTracking: false,
					}
				},
				series: [{
					type: 'spline',
					data: [
						{y:parseInt($('.table-popup.open .graph-table tr.total:eq(0) td:eq(1) input').val())},
						{y:parseInt($('.table-popup.open .graph-table tr.total:eq(0) td:eq(1) input').val())*1.75,color: 'transparent'},
						{y:parseInt($('.table-popup.open .graph-table tr.total:eq(0) td:eq(2) input').val())},
						{y:parseInt($('.table-popup.open .graph-table tr.total:eq(0) td:eq(2) input').val())*1.55,color: 'transparent'},
						{y:parseInt($('.table-popup.open .graph-table tr.total:eq(0) td:eq(3) input').val())}
					],
				}]
			});
		}
		$(this).removeClass('open');
	}).on('click', '.table-popup.open .graph-table', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		return false;
	}).on('change', '.graph-table input', function(e){
		redraw = true;
		const regex = /\d/gm;
		const str = $(this).val();
		let m;
		let new_str ='';
		while ((m = regex.exec(str)) !== null) {
			if (m.index === regex.lastIndex) {
				regex.lastIndex++;
			}
			m.forEach(function(match, groupIndex) {
				if(match != undefined){
				new_str+=match+='';
			}
		});
		}
		$(this).val(new Intl.NumberFormat('de-DE').format(new_str).split('.').join(','));
		var index = $(this).parent().index()+1;
		var total = 0;
		var gross_total = 0;
		var fire_total = 0;
		$(this).val().replace('/w+/');
		$('.table-popup.open .graph-table tr:not(.total) td:nth-child('+index+') input').each(function () {
			if($(this).val() != ''){
				total+= parseInt($(this).val().split(',').join(''));
			}
		});
		$('.table-popup.open .graph-table tr.total:eq(0) td:eq('+(index-1)+') input').val(new Intl.NumberFormat('de-DE').format(total).split('.').join(','));
		$('.table-popup.open .graph-table tr.total:eq(1) td:eq('+(index-1)+') input').val(new Intl.NumberFormat('de-DE').format(parseInt(total*0.56)).split('.').join(','));
		$('.table-popup.open .graph-table tr.total:eq(0) td input').each(function () {
			if($(this).val() != ''){
				gross_total+= parseInt($(this).val().split(',').join(''));
			}
		});
		$('.table-popup.open .graph-table tr.total:eq(0) td:nth-child(5)').text(new Intl.NumberFormat('de-DE').format(gross_total).split('.').join(','));
		$('.table-popup.open .graph-table tr.total:eq(1) td input').each(function () {
			if($(this).val() != ''){
				fire_total+= parseInt($(this).val().split(',').join(''));
			}
		});
		$('.table-popup.open .graph-table tr.total:eq(1) td:nth-child(5)').text(new Intl.NumberFormat('de-DE').format(fire_total).split('.').join(','));

		return false;
	});
});